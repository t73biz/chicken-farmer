# Chicken Farmer

A sample application to show my skills of development.

## Objective

To create a chicken farming simulation. This should simulate the growth cycles of chickens, from the laying of eggs to
the hatching of them. This will also track the amount of feed a farmer has, as well as the money available for buying feed.
As time progresses, at an accelerated rate, the chicks will become chickens and begin to produce eggs. The chicks, eggs,
and chickens can all be sold for feed, to progress through the simulation. The goal is to make a million dollars as a chicken farmer.

## Use Cases
* As a public user, I should be able to take on the role of a chicken farmer
* As a farmer, I should be able to buy chickens.
* As a farmer, I should be able to buy chicks.
* As a farmer, I should be able to buy chick brooders.
* As a farmer, I should be able to buy feed.
* As a farmer, I should be able to buy roosts (hen houses).
* As a farmer, I should be able to buy incubators.
* As a farmer, I should be able to feed chickens.
* As a farmer, I should be able to incubate eggs.
* As a farmer, I should be able to sell chicks.
* As a farmer, I should be able to sell chickens.
* As a farmer, I should be able to sell chick brooders.
* As a farmer, I should be able to sell eggs.
* As a farmer, I should be able to buy roosts (hen houses).
* As a farmer, I should be able to sell incubators.
* As a hen, I should be able to lay eggs, if I am younger than 5 years, once a day.
* As a rooster, I should be able to fertilize an egg, if I am younger than 5 years, once a day, at most.
* As a chicken, I should have a place to roost and lay eggs.
* As a chicken, if I don't have a place to roost for 1 months, I will die.
* As an egg, if I am left unattended for more than a day, I will rot.
* As an egg, if I am incubated for 30 days, I will hatch.
* As a hen, I can incubate up to 3 eggs.
* As a chicken, I will die after 7 years.
* As a chick, I will die after 1 day, if I don't have a brooder.
* As a chick, I will become a full grown chicken after 4 months.

## Objects

The following objects are used in the application:

* Brooder
    * id        - integer - The id of the brooder. (internal tracking)
    * farmer_id - integer - The id of the farmer. (internal tracking)
    * capacity  - integer - The amount of chicks that can be in the brooder.

* Chicken
    * id         - integer - The id of the chicken. (internal tracking)
    * brooder_id - integer - The assigned roost of the chicken.
    * farmer_id  - integer - The id of the farmer.  (internal tracking)
    * roost_id   - integer - The assigned roost of the chicken.
    * age        - integer - How old is the chicken.
    * sex        - varchar - Is the chicken male or female.
    * created    - date    - The date that the chicken was hatched. 
    
* Egg
    * id            - integer - The id of the egg. (internal tracking)
    * farmer_id     - integer - The id of the farmer.  (internal tracking)
    * age           - integer - The age of the egg.
    * is_fertilized - boolean - Is the egg fertilized.
    * is_hatched    - boolean - Has the egg hatched into a chick.
    * is_rotten     - boolean - Has the egg become rotten.
    * created       - date    - The date that the egg was laid. 

* Farmer
    * id   - integer - The id of the farmer. (internal tracking)
    * name - varchar - The name of the farmer.
    * cash - integer - The amount of money available to the farmer.
    * feed - integer - the amount of feed available to the farmer.

* Roost
    * id        - integer - The id of the roost. (internal tracking)
    * farmer_id - integer - The id of the farmer. (internal tracking)
    * capacity  - integer - The amount of chickens that can roost inside.
    
* Incubator
    * id        - integer - The id of the roost. (internal tracking)
    * farmer_id - integer - The id of the farmer. (internal tracking)
    * capacity  - integer - The amount of eggs that can incubate inside.


